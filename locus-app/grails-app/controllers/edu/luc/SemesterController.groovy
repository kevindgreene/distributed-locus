package edu.luc



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SemesterController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Semester.list(params), model: [semesterCount: Semester.count()]
    }

    def show(Semester semester) {
        respond semester
    }

    def create() {
        respond new Semester(params)
    }

    @Transactional
    def save(Semester semester) {
        if (!semester) {
            notFound()
            return
        }

        if (semester.hasErrors()) {
            respond semester.errors, view: 'create'
            return
        }

        semester.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'semester.label', default: 'Semester'), semester.id])
                redirect semester
            }
            '*' { respond semester, [status: CREATED] }
        }
    }

    def edit(Semester semester) {
        respond semester
    }

    @Transactional
    def update(Semester semester) {
        if (!semester) {
            notFound()
            return
        }

        if (semester.hasErrors()) {
            respond semester.errors, view: 'edit'
            return
        }

        semester.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Semester.label', default: 'Semester'), semester.id])
                redirect semester
            }
            '*' { respond semester, [status: OK] }
        }
    }

    @Transactional
    def delete(Semester semester) {

        if (!semester) {
            notFound()
            return
        }

        semester.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Semester.label', default: 'Semester'), semester.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'semester.label', default: 'Semester'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
