package edu.luc

import org.springframework.web.multipart.MultipartFile

class ImportController {
    def importService

    def upload() {
        log.debug "Processing Info"
        MultipartFile f = request.getFile("csv")
        if (f.empty) {
            flash.message = 'file cannot be empty'
            render(view: 'index')
            return
        }

        importService.importFile(f)
        response.sendError(200, 'Done')
    }

    def index() {}
}
