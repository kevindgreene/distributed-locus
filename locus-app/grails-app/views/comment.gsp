<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Distributed LOCUS">
    <meta name="author" content="Mary Mistretta, Kevin Greene, Joseph Reiter">
    <link rel="shortcut icon" href="docs-assets/ico/favicon.png">

    <title>Locus</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">
        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="home.gsp">Locus</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="home.gsp">Home</a></li>
                <li><a href="#">Search Courses</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="">Comment</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Actions <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Create New User</a></li>
                    <li><a href="#">Create New Course</a></li>
                    <li><a href="#">Create New Section</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
<div class="container marketing">
	  <hr class="featurette-divider">
      <!-- START THE FEATURETTES -->

      <div class="row featurette">
        <div class="col-md-5">
          <img class="featurette-image img-responsive" src="images/contact.jpg" alt="Contact Us">
        </div>
        <div class="col-md-7">
          <h2 class="featurette-heading">Leave us a Comment!<br><span class="text-muted">We love questions!</span></h2>
          <p class="lead">	  
          <!-- begin htmlcommentbox.com -->
		<div id="HCB_comment_box">
		<a href="http://www.htmlcommentbox.com">Comment Form</a> is loading comments...</div>
 			<link rel="stylesheet" type="text/css" href="//www.htmlcommentbox.com/static/skins/bootstrap/twitter-bootstrap.css?v=0" />
					<script type="text/javascript" id="hcb">
					/*<!--*/ if(!window.hcb_user){hcb_user={};} (function(){var s=document.createElement("script"), l=(hcb_user.PAGE || ""+window.location), h="//www.htmlcommentbox.com";s.setAttribute("type","text/javascript");s.setAttribute("src", h+"/jread?page="+encodeURIComponent(l).replace("+","%2B")+"&opts=16862&num=10");if (typeof s!="undefined") document.getElementsByTagName("head")[0].appendChild(s);})(); /*-->*/ </script>
					<!-- end htmlcommentbox.com --></p>
        </div>
      </div>

	<hr class="featurette-divider">
	</div>
      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>Photo Credit to: <a href="https://www.freedigitalphotos.net/">www.freedigitalphotos.net</a></p>
        <p>created by: Mary Mistretta, Kevin Green, Joseph Reiter</p>
        
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="docs-assets/js/holder.js"></script>
  </body>
</html>
