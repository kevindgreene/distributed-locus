<%@ page import="edu.luc.Course" %>



<div class="fieldcontain ${hasErrors(bean: courseInstance, field: 'department', 'error')} ">
	<label for="department">
		<g:message code="course.department.label" default="Department" />
		
	</label>
	<g:textField name="department" value="${courseInstance?.department}" />
</div>

<div class="fieldcontain ${hasErrors(bean: courseInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="course.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${courseInstance?.description}" />
</div>

<div class="fieldcontain ${hasErrors(bean: courseInstance, field: 'lucId', 'error')} ">
	<label for="lucId">
		<g:message code="course.lucId.label" default="Luc Id" />
		
	</label>
	<g:textField name="lucId" value="${courseInstance?.lucId}" />
</div>

<div class="fieldcontain ${hasErrors(bean: courseInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="course.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${courseInstance?.name}" />
</div>

