
<%@ page import="edu.luc.Section" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'section.label', default: 'Section')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-section" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-section" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list section">
			
				<g:if test="${sectionInstance?.course}">
				<li class="fieldcontain">
					<span id="course-label" class="property-label"><g:message code="section.course.label" default="Course" /></span>
					
						<span class="property-value" aria-labelledby="course-label"><g:link controller="course" action="show" id="${sectionInstance?.course?.id}">${sectionInstance?.course?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${sectionInstance?.courseNumber}">
				<li class="fieldcontain">
					<span id="courseNumber-label" class="property-label"><g:message code="section.courseNumber.label" default="Course Number" /></span>
					
						<span class="property-value" aria-labelledby="courseNumber-label"><g:fieldValue bean="${sectionInstance}" field="courseNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${sectionInstance?.lucSectionId}">
				<li class="fieldcontain">
					<span id="lucSectionId-label" class="property-label"><g:message code="section.lucSectionId.label" default="Luc Section Id" /></span>
					
						<span class="property-value" aria-labelledby="lucSectionId-label"><g:fieldValue bean="${sectionInstance}" field="lucSectionId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${sectionInstance?.maxSeats}">
				<li class="fieldcontain">
					<span id="maxSeats-label" class="property-label"><g:message code="section.maxSeats.label" default="Max Seats" /></span>
					
						<span class="property-value" aria-labelledby="maxSeats-label"><g:fieldValue bean="${sectionInstance}" field="maxSeats"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${sectionInstance?.professor}">
				<li class="fieldcontain">
					<span id="professor-label" class="property-label"><g:message code="section.professor.label" default="Professor" /></span>
					
						<span class="property-value" aria-labelledby="professor-label"><g:link controller="user" action="show" id="${sectionInstance?.professor?.id}">${sectionInstance?.professor?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${sectionInstance?.semester}">
				<li class="fieldcontain">
					<span id="semester-label" class="property-label"><g:message code="section.semester.label" default="Semester" /></span>
					
						<span class="property-value" aria-labelledby="semester-label"><g:link controller="semester" action="show" id="${sectionInstance?.semester?.id}">${sectionInstance?.semester?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:sectionInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${sectionInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
