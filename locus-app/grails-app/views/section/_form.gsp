<%@ page import="edu.luc.Section" %>



<div class="fieldcontain ${hasErrors(bean: sectionInstance, field: 'course', 'error')} ">
	<label for="course">
		<g:message code="section.course.label" default="Course" />
		
	</label>
	<g:select id="course" name="course.id" from="${edu.luc.Course.list()}" optionKey="id" required="" value="${sectionInstance?.course?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sectionInstance, field: 'courseNumber', 'error')} ">
	<label for="courseNumber">
		<g:message code="section.courseNumber.label" default="Course Number" />
		
	</label>
	<g:field type="number" name="courseNumber" value="${sectionInstance.courseNumber}" />
</div>

<div class="fieldcontain ${hasErrors(bean: sectionInstance, field: 'lucSectionId', 'error')} ">
	<label for="lucSectionId">
		<g:message code="section.lucSectionId.label" default="Luc Section Id" />
		
	</label>
	<g:textField name="lucSectionId" value="${sectionInstance?.lucSectionId}" />
</div>

<div class="fieldcontain ${hasErrors(bean: sectionInstance, field: 'maxSeats', 'error')} ">
	<label for="maxSeats">
		<g:message code="section.maxSeats.label" default="Max Seats" />
		
	</label>
	<g:field type="number" name="maxSeats" value="${sectionInstance.maxSeats}" />
</div>

<div class="fieldcontain ${hasErrors(bean: sectionInstance, field: 'professor', 'error')} ">
	<label for="professor">
		<g:message code="section.professor.label" default="Professor" />
		
	</label>
	<g:select id="professor" name="professor.id" from="${edu.luc.User.list()}" optionKey="id" required="" value="${sectionInstance?.professor?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sectionInstance, field: 'semester', 'error')} ">
	<label for="semester">
		<g:message code="section.semester.label" default="Semester" />
		
	</label>
	<g:select id="semester" name="semester.id" from="${edu.luc.Semester.list()}" optionKey="id" required="" value="${sectionInstance?.semester?.id}" class="many-to-one"/>
</div>

