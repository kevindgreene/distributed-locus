
<%@ page import="edu.luc.Section" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'section.label', default: 'Section')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-section" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-section" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="section.course.label" default="Course" /></th>
					
						<g:sortableColumn property="courseNumber" title="${message(code: 'section.courseNumber.label', default: 'Course Number')}" />
					
						<g:sortableColumn property="lucSectionId" title="${message(code: 'section.lucSectionId.label', default: 'Luc Section Id')}" />
					
						<g:sortableColumn property="maxSeats" title="${message(code: 'section.maxSeats.label', default: 'Max Seats')}" />
					
						<th><g:message code="section.professor.label" default="Professor" /></th>
					
						<th><g:message code="section.semester.label" default="Semester" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${sectionInstanceList}" status="i" var="sectionInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${sectionInstance.id}">${fieldValue(bean: sectionInstance, field: "course")}</g:link></td>
					
						<td>${fieldValue(bean: sectionInstance, field: "courseNumber")}</td>
					
						<td>${fieldValue(bean: sectionInstance, field: "lucSectionId")}</td>
					
						<td>${fieldValue(bean: sectionInstance, field: "maxSeats")}</td>
					
						<td>${fieldValue(bean: sectionInstance, field: "professor")}</td>
					
						<td>${fieldValue(bean: sectionInstance, field: "semester")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${sectionInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
