<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Distributed LOCUS">
    <meta name="author" content="Mary Mistretta, Kevin Greene, Joseph Reiter">
    <link rel="shortcut icon" href="docs-assets/ico/favicon.png">

    <title>Locus</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">
        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Locus</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">Search Courses</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="comment.gsp">Comment</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Actions <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Create New User</a></li>
                    <li><a href="#">Create New Course</a></li>
                    <li><a href="#">Create New Section</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <img class="img-circle" src="images/awards.jpg" alt="Generic placeholder image">
          <h2>Awards</h2>
          <p>At this time the site has not received any awards.</p>
          <p><a class="btn btn-default" href="comment.gsp" role="button">Comment &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="images/standOut.jpg" alt="Generic placeholder image">
          <h2>Course Offerings</h2>
          <p>Want a different course offered? Want something during a different semester?</p>
          <p><a class="btn btn-default" href="comment.gsp" role="button">Comment &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="images/input2.jpg" alt="Generic placeholder image">
          <h2>Suggestions</h2>
          <p>Your input is valued here at Locus. We would love to hear your thoughts on the site.</p>
          <p><a class="btn btn-default" href="comment.gsp" role="button">Comment &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-5">
          <img class="featurette-image img-responsive" src="images/contact.jpg" alt="Contact Us">
        </div>
        <div class="col-md-7">
        <a id="contact">
          <h2 class="featurette-heading">Contact Us<br><span class="text-muted">We love questions!</span></h2></a>
          <p class="lead">E-mail or commenting on the comment board is the preferred method of contacting us.  Please send e-mails to: mary.mistretta@gmail.com</p>
        </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>Photo Credit to: <a href="https://www.freedigitalphotos.net/">www.freedigitalphotos.net</a></p>
        <p>created by: Mary Mistretta, Kevin Greene, Joseph Reiter</p>
        
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="docs-assets/js/holder.js"></script>
  </body>
</html>
