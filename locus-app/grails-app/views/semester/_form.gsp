<%@ page import="edu.luc.Semester" %>



<div class="fieldcontain ${hasErrors(bean: semesterInstance, field: 'season', 'error')} ">
	<label for="season">
		<g:message code="semester.season.label" default="Season" />
		
	</label>
	<g:select name="season" from="${edu.luc.Season?.values()}" keys="${edu.luc.Season.values()*.name()}" required="" value="${semesterInstance?.season?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: semesterInstance, field: 'year', 'error')} ">
	<label for="year">
		<g:message code="semester.year.label" default="Year" />
		
	</label>
	<g:field type="number" name="year" value="${semesterInstance.year}" />
</div>

