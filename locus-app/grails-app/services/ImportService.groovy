import au.com.bytecode.opencsv.*
import com.mongodb.*
import edu.luc.Course
import edu.luc.Section
import edu.luc.User
import org.springframework.web.multipart.MultipartFile;

class ImportService {
    def elasticsearchIndexService

    def importFile(MultipartFile csvFile) {
        log.info "Hit the service"


        CSVReader reader = new CSVReader(new InputStreamReader(csvFile.inputStream));
        String[] nextLine;
        reader.readNext()
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] is an array of values from the line
            if (nextLine.size() != 11) {
                log.info "ERROR WITH IMPORT SIZE"
                break;
            }

            Course course
            Section section
            User user

            course = Course.findByName(nextLine[2]) ?: new Course()
            course.name = nextLine[2]
            course.department = nextLine[0]
            course.lucId = nextLine[1]
            course.description = "Currently no description is provided"
            course.save(flush: true)

            user = User.findByFirstNameAndLastName(nextLine[5], nextLine[6]) ?: new User()
            user.firstName = nextLine[5]
            user.lastName = nextLine[6]
            user.save(flush: true)


            section = Section.findByLucSectionIdAndCourse(nextLine[4], course) ?: new Section()
            section.lucSectionId = nextLine[4]
            section.course = course
            section.maxSeats = nextLine[7] as Integer
            section.enrolled = nextLine[8] as Integer
            section.courseNumber = nextLine[10] as Integer
            section.professor = user
            section.save(flush: true)


            elasticsearchIndexService.indexClass(section)

            log.info "Inserting record for ${course.name}"
        }
    }
}
