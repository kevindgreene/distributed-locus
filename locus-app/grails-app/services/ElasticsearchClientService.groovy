import org.codehaus.groovy.grails.commons.DefaultGrailsApplication
import org.elasticsearch.groovy.client.GClient
import org.elasticsearch.groovy.node.GNode
import org.elasticsearch.groovy.node.GNodeBuilder
import org.elasticsearch.client.Requests
import static org.elasticsearch.groovy.node.GNodeBuilder.nodeBuilder

import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

class ElasticsearchClientService {
    GClient gclient
    GNode gnode
    DefaultGrailsApplication grailsApplication

    @PostConstruct
    void init() {
        GNodeBuilder nodeBuilder = nodeBuilder()
        nodeBuilder.settings {
            node { client = true }
            cluster {name ="locus"}
            discovery {
                zen {
                    ping {
                        multicast { enabled = false }
                        unicast { hosts = ['dev.locus.internal:9300'] }
                    }
                }
            }

        }


        gnode = nodeBuilder.node()
        gclient = gnode.getClient()
    }

    @PreDestroy
    void destroy() {
        log.info("Closing node: $gnode")
        gnode?.close()
    }

    boolean indexExists(String indexName) {
        gclient.admin.indices.indicesAdminClient.exists(Requests.indicesExistsRequest(indexName)).get().exists
    }

}
