import edu.luc.Course
import edu.luc.Section

class ElasticsearchIndexService {
    def elasticsearchClientService

    def indexClass(Section section) {
        def client = elasticsearchClientService.gclient
        def indexR = client.index {
            index "luc"
            type "class"
            id section.id.toString()
            source {
                lucId = section.course.lucId
                name = section.course.name
                department = section.course.department
                professor = section.professor.firstName + " " + section.professor.lastName
                maxSeats = section.maxSeats
                currentSeats = section.maxSeats - section.enrolled
                courseNumber = section.courseNumber
            }
        }
    }
}
