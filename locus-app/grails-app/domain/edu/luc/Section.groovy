package edu.luc

import org.bson.types.ObjectId
/*
 * A section is a specific instance of a course.
 * It is identified by the course and also by a section id.
 * Users enroll in sections of courses. 
 */

class Section {

    ObjectId id
    String lucSectionId
    int courseNumber

    Course course
    int maxSeats
    int enrolled

    User professor
}
