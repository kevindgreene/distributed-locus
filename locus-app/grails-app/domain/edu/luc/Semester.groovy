package edu.luc

import org.bson.types.ObjectId
/*
 * Courses are offered in different semesters. 
 * At Loyola these semesters take place during the 
 * Fall (Fall semester), Winter (J-term), Spring 
 * (Spring semester), or Summer (multiple Summer sessions).
 */

class Semester {

    ObjectId id

    Season season
    int year
}
