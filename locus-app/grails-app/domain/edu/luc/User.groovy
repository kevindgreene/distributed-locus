package edu.luc

import org.bson.types.ObjectId
/*
 * Users can be students, professors, or administrators.  
 * Student users enroll in classes. 
 */

class User {

    ObjectId id
    String firstName
    String lastName
}
