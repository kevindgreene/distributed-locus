package edu.luc

import org.bson.types.ObjectId
/*
 * A course is identified by its name and id. 
 * Sections of the course are what users actually enroll in. 
 */

class Course {

    ObjectId id

    String description
    String lucId
    String name
    String department
}
