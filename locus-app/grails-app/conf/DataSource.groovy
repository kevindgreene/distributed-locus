import org.bson.types.ObjectId

grails {
    mongo {
        host = "dev.locus.internal"
        databaseName = "locus"
    }
}

environments {
    test {
        grails {
            mongo {
                databaseName = "locus_test_${new ObjectId().toString()}"
                dropOnDestroy = true
            }
        }
    }
}