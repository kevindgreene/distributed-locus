exec { "apt-get update":
    command => "/usr/bin/apt-get update",
    before => Class['java']
}

wget::fetch { "download elasticsearch Deb":
       source      => 'https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-0.90.6.deb',
       destination => '/tmp/elasticsearch-0.90.6.deb',
       timeout     => 0,
       verbose     => false,
       before => Class['elasticsearch']
}

class { 'java':
       before => Class['elasticsearch']
}


class { 'elasticsearch':
   pkg_source => '/tmp/elasticsearch-0.90.6.deb',
   config =>{
      'cluster'            => {
        'name'             => 'locus'
      },
      'network'            => {
        'publish_host'     => '192.168.103.100'
      }
  }
}
class { 'mongodb':
    ulimit_nofile => 20000,
}

 elasticsearch::plugin{'mobz/elasticsearch-head':
   module_dir => 'head'
 }
